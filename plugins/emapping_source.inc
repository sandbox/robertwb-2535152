<?php
class eMappingSourceDefaultHandler {
  var $query = '';
  var $options = array();
  var $formats = array();
  var $data_uri; // store things like raw data, json, or even SQL to describe a source
  var $name;
  var $default_format;
  var $type;
  
  public function __construct() {
    $this->formats = 'text';
    $this->default_format = 'text';
  }
  
  public function init($options) {
    foreach ($this->option_definition() as $opt => $def) {
      if (isset($options[$opt])) {
        $this->options[$opt] = $options[$opt];
      }
    }
  }

  function option_definition() {
    $options['enabled'] = array('default' => TRUE);
    return $options;
  }
  
  //public function buildOptionsForm(&$form, FormStateInterface $form_state) {
  // wqhen we go to D8 this will be relevant
  public function buildOptionsForm(&$form, $form_state) {
    // classes that inherit this will call the parent, but since this is the parent - don't
    // parent::buildOptionsForm($form, $form_state);
    // allow + for or, , for and
    $form['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#description' => t('If selected, this source is enabled'),
      '#default_value' => !empty($this->options['enabled']),
    );
    // this is an object prop, so don't show it here?
    //$form['data_uri'] = array(
    //  '#title' => t('Data or Location of source'),
    //  '#type' => 'textarea',
    //  '#default_value' => $this->options['enabled'],
    //  '#description' => t('URI or Text data, WKT or structured such as JSON.'),
    //  '#required' => FALSE,
    //  '#size' => 5,
    //);
  }
  
  public function query() {
    // sets the object query and retrieves data
    $this->data = $this->data_uri;
  }
  
  public function getData($format = NULL) {
    // returns formatted data
    // returns data in the requested format if supported
    // if does not support requested format, set log error and return FALSE
    // this must know how to translate into another format if it does so
    // generally, we will avoid translating here, and allow translation at the layer level,
    // but for example, we might support a Views data as an array, or as a cache object reference
    $format = ($format === NULL) ? $this->default_format : $format;
    switch ($format) {
      default:
        return $this->data;
      break;
    }
  }
  
  public function getFormats() {
    // returns an array of data formats this can return
    return $this->formats();
  }
}

class eMappingSourceViewsHandler extends eMappingSourceDefaultHandler {
  
  public function __construct() {
    $this->formats = array('array_assoc', 'views_cache', 'sql');
    $this->default_format = 'array_assoc';
    $this->data = array();
  }
  

  function option_definition() {
    $options = parent::option_definition();

    $options['view'] = array('default' => FALSE);
    $options['display'] = array('default' => FALSE);
    $options['arguments'] = array('default' => FALSE);

    return $options;
  }
  // when we go to D8 this will be relevant
    // public function buildOptionsForm(&$form, FormStateInterface $form_state) {
  // until then, we use the old school method
  public function buildOptionsForm(&$form, $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // get all views as an options form -- this came from views_field_view module
    // this is not yet tested, we may need to insure that all the views functions are actually available to us
    $view_options = views_get_views_as_options(TRUE, 'all', NULL, FALSE, TRUE);
    $hidden = array('data_uri');
    foreach ($hidden as $hidethis) {
      $form[$hidethis]['#type'] = 'hidden';
    }
    $form['views_field_view'] = array(
      '#type' => 'fieldset',
      '#title' => t("View settings"),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['view'] = array(
      '#type' => 'select',
      '#title' => t('View'),
      '#description' => t('Select a view to embed.'),
      '#default_value' => $this->options['view'],
      '#options' => $view_options,
      '#ajax' => array(
        'callback' => 'emapping_display_update',
        'wrapper' => 'update-display-value',
      ),
      '#fieldset' => 'views_field_view',
    );

    // If there is no view set, use the first one for now.
    if (count($view_options) && empty($this->options['view'])) {
      $this->options['view'] = reset(array_keys($view_options));
    }

    if ($this->options['view']) {
      $view = views_get_view($this->options['view']);

      $display_options = array();
      foreach ($view->display as $name => $display) {
        // Allow to embed a different display as the current one.
        if ($this->options['view'] != $this->view->name || ($this->view->current_display != $name)) {
          $display_options[$name] = $display->display_title;
        }
      }

      $form['display'] = array(
        '#type' => 'select',
        '#title' => t('Display'),
        '#description' => t('Select a view display to use.'),
        '#default_value' => $this->options['display'],
        '#options' => $display_options,
        '#fieldset' => 'views_field_view',
        '#prefix' => "<div id='update-display-value'>",
        '#suffix' => "</div>",
      );

      $form['arguments'] = array(
        '#title' => t('Contextual filters'),
        '#description' => t('(not yet working) Use a comma (,) or forwardslash (/) separated list of each contextual filter which should be forwared to the view. 
          See below list of available replacement tokens. Static values are also be passed to child views if they do not match a token format. 
          You could pass static ID\'s or taxonomy terms in this way. E.g. 123 or "my taxonomy term".'),
        '#type' => 'textfield',
        '#default_value' => $this->options['arguments'],
        '#fieldset' => 'views_field_view',
        '#maxlength' => '256',
      );
    }
    // END STUFF PURLOINED FROM views_field_view
  }
  
  public function query() {
    // instantiate the view object
    // add any contextual filters that the options form allows
    // query the view
    // set the views data into $this->data using $view->style_plugin->render_fields($view->result);
    $this->data_uri = 'put the views SQL here';
  }
  
  public function getData($format = NULL) {
    return $this->data;
  }
  
  public function getFormats() {
    return $this->formats();
  }
}


class eMappingSourceSQLHandler extends eMappingSourceDefaultHandler {
  var $host;
  var $port;
  var $dbuser;
  var $dbname;
  var $userpass;
  var $sql;
  
  public function __construct() {
    $this->formats = array('array_assoc', 'result_object', 'sql');
    $this->default_format = 'array_assoc';
    $this->data = array();
  }
  
  function option_definition() {
    $options = parent::option_definition();

    $options['sql'] = array('default' => '');
    $options['host'] = array('default' => '');
    $options['port'] = array('default' => '');
    $options['dbtype'] = array('default' => '');
    $options['dbuser'] = array('default' => '');
    $options['dbname'] = array('default' => '');
    $options['userpass'] = array('default' => '');

    return $options;
  }
  // when we go to D8 this will be relevant
    // public function buildOptionsForm(&$form, FormStateInterface $form_state) {
  // until then, we use the old school method
  public function buildOptionsForm(&$form, $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $hidden = array('data_uri');
    foreach ($hidden as $hidethis) {
      $form[$hidethis]['#type'] = 'hidden';
    }
    // enter SQL in text field
    $form['sql'] = array(
      '#title' => t('SQL Query'),
      '#description' => t('Query to run to obtain data.'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#default_value' => $this->options['sql'],
      '#size' => 5,
    );
    $form['available_tokens'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns (not yet working)'),
      '#description' => t('Need to replace array() in #value below with suitable code - decide on either ctools tokens or tokens module tokens, or both?'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => array(),
      '#fieldset' => 'views_field_view',
    );
    $form['host'] = array(
      '#title' => t('Host'),
      '#type' => 'textfield',
      '#default_value' => $this->options['host'],
      '#description' => t('Host (optional) - if omitted, local Database query will be attempted.'),
      '#required' => FALSE,
      '#size' => 64,
    );  
    // enter SQL in text field
    $form['port'] = array(
      '#title' => t('Port'),
      '#type' => 'textfield',
      '#default_value' => $this->options['port'],
      '#description' => t('port (optional) - only needed in case of remote query.'),
      '#required' => FALSE,
      '#size' => 12,
    );  
    $form['dbname'] = array(
      '#title' => t('Database Name'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dbname'],
      '#description' => t('Database Name (optional) - only needed in case of remote query.'),
      '#required' => FALSE,
      '#size' => 32,
    );  
    $form['dbuser'] = array(
      '#title' => t('DB User'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dbuser'],
      '#description' => t('DB User Name (optional) - only needed in case of remote query.'),
      '#required' => FALSE,
      '#size' => 32,
    );  
    // enter SQL in text field
    $form['userpass'] = array(
      '#title' => t('Password'),
      '#type' => 'password',
      '#default_value' => $this->options['userpass'],
      '#description' => t('password (optional) - only needed in case of remote query.'),
      '#required' => FALSE,
      '#size' => 32,
    );  
  }
  
  public function query() {
    // instantiate the view object
    // add any contextual filters that the options form allows
    // query the view
    // set the views data into $this->data
    $this->sql = isset($this->options['sql']) ? $this->options['sql'] : $this->data_uri;
    $this->sql = token_replace($this->sql, array(), array('clear'=>TRUE));
    $this->data_uri = $this->sql;
  }
  
  public function getData($format = NULL) {
    return $this->data;
  }
  
  public function getFormats() {
    return $this->formats();
  }
}