<?php

class EMappingLayerShapeFileHandler extends EMappingLayerHandlerDefault {
  var $files = array();
  var $out_dir = '';
  var $file_base = '';
  var $exts = array('shp', 'dbf', 'prj', 'shx');
  
  function render() {
    if (is_object($this->layer_entity)) {
      $src = array_shift($this->layer_entity->getLayerSource());
      // choose a file name from the source machine name - later will add that as a setting in the handler object
      $this->file_base = $src->machine_name;
      $filename = $src->machine_name . ".shp";
      $delname = $src->machine_name . ".*";
      //drupal_set_message("Source: " . print_r((array)$src,1));
      $opts = $src->options;
      //dpm(
      $opts['sql'] = token_replace($opts['sql'], array(), array('clear'=>TRUE));
      $conn = "host=$opts[host] user=$opts[dbuser] dbname=$opts[dbname] password=$opts[userpass]";
      // format the ogr2ogr command with the filename
      $cmd = "cd $this->out_dir; rm $delname ; ";
      $cmd .= "ogr2ogr -a_srs EPSG:4326 -f \"ESRI Shapefile\" $filename PG:\"$conn\" -sql \"$opts[sql]\" ";
      // preview the command
      if (function_exists('dpm')) {
        dpm($cmd, "Export Command");
      }
      shell_exec($cmd);
    }
  }
    
  function getFiles() {
    // special method supported by layers that can be used with map type = emapping_archive
    $files = array();
    foreach ($this->exts as $ext) {
      if (file_exists($this->out_dir . "/" . $this->file_base . ".$ext")) {
        $files[] = $this->file_base . ".$ext";
      }
    }
    return $files;
  }

};

?>