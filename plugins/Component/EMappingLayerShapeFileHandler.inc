<?php

$plugin = array(
  'label' => t('Renders data source to ESRI shape file.'),
  'title' => t('ESRI shapefile'),
  'plugin_name' => t('layer_esri_shapefile'),
  'display_modules' => array('emapping_archive'),
  'source_classes' => array('eMappingSourceSQLHandler'),
  'handler' =>  array(
    'class' => 'EMappingLayerShapeFileHandler',
    'file' => 'EMappingLayerShapeFileHandler.class.php',
  ),
);

?>