<?php

$plugin = array(
  'label' => t('eMapping Archive Map Display Handler'),
  'title' => t('Archive File'),
  'plugin_name' => t('map_archive_file'),
  'handler' =>  array(
    'class' => 'emappingArchiveHandlerDefault',
    'file' => 'emappingArchiveHandlerDefault.class.php',
  ),
);

?>