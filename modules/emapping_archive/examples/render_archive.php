<?php
$a = arg();
dpm($a, "Args: ");
if (isset($a[2])) {
  $mid = $a[2];
} else {
  $mid = 0;
}
// prototype for emapping_archive_render() & emappingArchiveHandlerDefault
//  ** actually, emapping_archive_render may not be needed
//     it could be done in emapping_display_render() with plugin calls
// load the desired map - this gets us all of our sources
// currently, the map will give us our layers as well, but
// this will soon be separated, where we load map, then load it's 
// display plugin
$map = entity_load_single('emapping_map', $mid);

if (!is_object($map)) {
  echo "Could not load object $mid - exiting.";
  //echo "getMapArray() = <pre>" . print_r($stuff,1) . "<pre>";
} else {
  ctools_include('plugins');
  $plugins = ctools_get_plugins('emapping', 'Map');
  dpm($plugins, "Map emapping plugins");
  $class = FALSE;
  $plugin = ctools_get_plugins('emapping', 'Map', 'emappingArchiveHandlerDefault');
  dpm($plugin, "emappingArchiveHandlerDefault plugin");
  $class = ctools_plugin_get_class($plugin, 'handler');
  dpm($class, "result of ctools_plugin_get_class($plugin, 'handler')");
  // iterate through the layers
  if ($class) {
    $archive = new $class;
    dpm($archive, "$class object");
    $archive->map_entity = $map;
    $archive->render();
    echo "Download: <a href='" . $archive->uri . "'>" . $archive->map_entity->name . "</a>";
  } else {
    echo "Could not load ctools plugin EMappingLayerShapeFileHandler";
  }
}
?>