<?php

/**
 * @file
 * Base class for map types.
 */

/**
 * Default Mapping Map Type Class
 *
 * This provides a default map type class so that map type plugins can be
 * extended with ctools.  See MappingBaseType class.
 */
class MappingMapType extends MappingBaseType {
  public $baseType = 'map';
  public $exportFields = array(
    'name',
    'title',
    'description',
    'data',
    'object_type',
    'layers',
    'styles',
    'behaviors',
  );

  // Array of layer names or export objects.
  public $layers = array();
  // Array of behavior names or export objects.
  public $behaviors = array();
  // Array of style names or export objects.
  public $styles = array();
}
