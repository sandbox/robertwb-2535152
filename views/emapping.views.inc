<?php

/**
 * Implements hook_views_handlers().
 */
function emapping_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'emapping') . '/views',
    ),
  );
}

/**
 * Implements hook_views_data().
 *
 */
//function emapping_views_data() {
//  $data = array();
//  return $data;
//}

/**
  * Implements hook_views_default_views().
  *
  */
function emapping_views_default_views() {
  $views = array();
  $views['emapping_map_info'] = emapping_map_info_view();
  $views['emapping_map_detail'] = emapping_map_detail_view();
  $views['emapping_map_add_layer'] = emapping_map_add_layer_view();
  return $views;
}

function emapping_map_info_view(){
  $view = new view();
  $view->name = 'emapping_map_info';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'emapping_map';
  $view->human_name = 'eMapping - Map Info';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'eMapping - Map Info';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: EMap: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'emapping_map';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Map Title';
  /* Field: EMap: Emap ID */
  $handler->display->display_options['fields']['mid']['id'] = 'mid';
  $handler->display->display_options['fields']['mid']['table'] = 'emapping_map';
  $handler->display->display_options['fields']['mid']['field'] = 'mid';
  $handler->display->display_options['fields']['mid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['mid']['alter']['text'] = '[mid] (edit)';
  $handler->display->display_options['fields']['mid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['mid']['alter']['path'] = 'admin/content/emapping_map/manage/[mid]';
  $handler->display->display_options['fields']['mid']['alter']['path_query_parameters'] = 'destination=emap/[mid]';
  $handler->display->display_options['fields']['mid']['separator'] = '';
  /* Field: EMap: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'emapping_map';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: EMap: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'emapping_map';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Contextual filter: EMap: Emap ID */
  $handler->display->display_options['arguments']['mid']['id'] = 'mid';
  $handler->display->display_options['arguments']['mid']['table'] = 'emapping_map';
  $handler->display->display_options['arguments']['mid']['field'] = 'mid';
  $handler->display->display_options['arguments']['mid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['mid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['mid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['mid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['mid']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'emap-info';
  return $view;
}
function emapping_map_detail_view(){
  $view = new view();
  $view->name = 'emapping_map_details';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'emapping_map';
  $view->human_name = 'eMapping - Map Details';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'eMapping - Map Detail';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: View area */
  $handler->display->display_options['header']['view']['id'] = 'view';
  $handler->display->display_options['header']['view']['table'] = 'views';
  $handler->display->display_options['header']['view']['field'] = 'view';
  $handler->display->display_options['header']['view']['empty'] = TRUE;
  $handler->display->display_options['header']['view']['view_to_insert'] = 'emapping_map_info:page';
  $handler->display->display_options['header']['view']['inherit_arguments'] = TRUE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<?php
    $links = array(
      "emap/!1/layer/add" => \'Add Layer\',
    );
    $ul = "<ul class=\'action-links\'>";
    foreach ($links as $path => $title) {
      $ul .= "<li>" . l($title, $path, array()) . "</li>";
    }
    $ul .= "</ul>";
    echo $ul;
    ?>';
  $handler->display->display_options['footer']['area']['format'] = 'php_code';
  $handler->display->display_options['footer']['area']['tokenize'] = TRUE;
  /* Relationship: Layers */
  $handler->display->display_options['relationships']['maplayer_target_id']['id'] = 'maplayer_target_id';
  $handler->display->display_options['relationships']['maplayer_target_id']['table'] = 'field_data_maplayer';
  $handler->display->display_options['relationships']['maplayer_target_id']['field'] = 'maplayer_target_id';
  $handler->display->display_options['relationships']['maplayer_target_id']['ui_name'] = 'Layers';
  $handler->display->display_options['relationships']['maplayer_target_id']['label'] = 'Layers';
  /* Relationship: Layer Source */
  $handler->display->display_options['relationships']['layersource_target_id']['id'] = 'layersource_target_id';
  $handler->display->display_options['relationships']['layersource_target_id']['table'] = 'field_data_layersource';
  $handler->display->display_options['relationships']['layersource_target_id']['field'] = 'layersource_target_id';
  $handler->display->display_options['relationships']['layersource_target_id']['relationship'] = 'maplayer_target_id';
  $handler->display->display_options['relationships']['layersource_target_id']['ui_name'] = 'Layer Source';
  $handler->display->display_options['relationships']['layersource_target_id']['label'] = 'Layer Source';
  /* Field: EMap: Emap ID */
  $handler->display->display_options['fields']['mid']['id'] = 'mid';
  $handler->display->display_options['fields']['mid']['table'] = 'emapping_map';
  $handler->display->display_options['fields']['mid']['field'] = 'mid';
  $handler->display->display_options['fields']['mid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['mid']['separator'] = '';
  /* Field: EMap Layer Class: Emap layer class ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'emapping_layer';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  $handler->display->display_options['fields']['lid']['relationship'] = 'maplayer_target_id';
  $handler->display->display_options['fields']['lid']['label'] = 'LayerID';
  $handler->display->display_options['fields']['lid']['separator'] = '';
  /* Field: EMap Layer Class: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'emapping_layer';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'maplayer_target_id';
  $handler->display->display_options['fields']['title']['label'] = 'Layer';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'admin/content/emapping_layer/manage/[lid]';
  $handler->display->display_options['fields']['title']['alter']['path_query_parameters'] = 'destination=emap/[mid]';
  /* Field: EMap Layer Class: Bundle */
  $handler->display->display_options['fields']['bundle']['id'] = 'bundle';
  $handler->display->display_options['fields']['bundle']['table'] = 'emapping_layer';
  $handler->display->display_options['fields']['bundle']['field'] = 'bundle';
  $handler->display->display_options['fields']['bundle']['relationship'] = 'maplayer_target_id';
  $handler->display->display_options['fields']['bundle']['label'] = 'Type';
  /* Field: EMap Source Class: Emap source class ID */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'emapping_source';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['relationship'] = 'layersource_target_id';
  $handler->display->display_options['fields']['sid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: EMap Source Class: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'emapping_source';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'layersource_target_id';
  $handler->display->display_options['fields']['name']['label'] = 'Source';
  $handler->display->display_options['fields']['name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['name']['alter']['path'] = 'admin/content/emapping_source/manage/[sid]';
  $handler->display->display_options['fields']['name']['alter']['path_query_parameters'] = 'destination=emap/[mid]';
  /* Contextual filter: EMap: Emap ID */
  $handler->display->display_options['arguments']['mid']['id'] = 'mid';
  $handler->display->display_options['arguments']['mid']['table'] = 'emapping_map';
  $handler->display->display_options['arguments']['mid']['field'] = 'mid';
  $handler->display->display_options['arguments']['mid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['mid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['mid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['mid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['mid']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'emap';

  return $view;
}
function emapping_map_add_layer_view(){
  $view = new view();
  $view->name = 'emapping_map_add_layer';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'emapping_layer_type';
  $view->human_name = 'eMapping - Map Add Layer';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'eMapping - Map Add Layer';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Emapping Layer Type: Internal, numeric emapping layer type ID */
  $handler->display->display_options['fields']['ltid']['id'] = 'ltid';
  $handler->display->display_options['fields']['ltid']['table'] = 'emapping_layer_type';
  $handler->display->display_options['fields']['ltid']['field'] = 'ltid';
  $handler->display->display_options['fields']['ltid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['ltid']['separator'] = '';
  /* Field: Emapping Layer Type: Label */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'emapping_layer_type';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Emapping Layer Type: Machine-readable name */
  $handler->display->display_options['fields']['bundle']['id'] = 'bundle';
  $handler->display->display_options['fields']['bundle']['table'] = 'emapping_layer_type';
  $handler->display->display_options['fields']['bundle']['field'] = 'bundle';
  $handler->display->display_options['fields']['bundle']['label'] = '';
  $handler->display->display_options['fields']['bundle']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['bundle']['alter']['text'] = 'Add [name]';
  $handler->display->display_options['fields']['bundle']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['bundle']['alter']['path'] = 'admin/content/emapping_layer/add/[bundle]';
  $handler->display->display_options['fields']['bundle']['alter']['path_query_parameters'] = 'mapid=!1&destination=emap/!1';
  $handler->display->display_options['fields']['bundle']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['bundle']['element_label_colon'] = FALSE;
  /* Sort criterion: Emapping Layer Type: Label */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'emapping_layer_type';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  /* Contextual filter: MapID Placeholder */
  $handler->display->display_options['arguments']['null']['id'] = 'null';
  $handler->display->display_options['arguments']['null']['table'] = 'views';
  $handler->display->display_options['arguments']['null']['field'] = 'null';
  $handler->display->display_options['arguments']['null']['ui_name'] = 'MapID Placeholder';
  $handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'emap/%/layer/add';

  return $view;
}
