<?php

module_load_include('inc', 'emapping', 'plugins/emapping_source');

class eMappingMapTypeUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage Map types, including adding	and removing fields and the display of fields.';
    return $items;
  }
}
class eMappingMapUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['type'] = MENU_LOCAL_TASK;	
    // Extend the 'add' path.
    $items[$this->path . '/add'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType),
      'page callback' => 'entity_ui_bundle_add_page',
      'page arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
      'type' => MENU_LOCAL_ACTION,
    );
    $items[$this->path . '/add/%'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType, $this->id_count + 1),
      'page callback' => 'entity_ui_get_bundle_add_form',
      'page arguments' => array($this->entityType, $this->id_count + 1),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
    );

    if (!empty($this->entityInfo['admin ui']['file'])) {
      // Add in the include file for the entity form.
      foreach (array('/add', '/add/%') as $path_end) {
        $items[$this->path . $path_end]['file'] = $this->entityInfo['admin ui']['file'];
        $items[$this->path . $path_end]['file path'] = isset($this->entityInfo['admin ui']['file path']) ? $this->entityInfo['admin ui']['file path'] : drupal_get_path('module', $this->entityInfo['module']);
      }
    }

    return $items;
  }
}

class eMappingLayerTypeUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage Layer types, including adding	and removing fields and the display of fields.';
    return $items;
  }
}

class eMappingLayerUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['type'] = MENU_LOCAL_TASK;	
    // Extend the 'add' path.
    $items[$this->path . '/add'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType),
      'page callback' => 'entity_ui_bundle_add_page',
      'page arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
      'type' => MENU_LOCAL_ACTION,
    );
    $items[$this->path . '/add/%'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType, $this->id_count + 1),
      'page callback' => 'entity_ui_get_bundle_add_form',
      'page arguments' => array($this->entityType, $this->id_count + 1),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
    );

    if (!empty($this->entityInfo['admin ui']['file'])) {
      // Add in the include file for the entity form.
      foreach (array('/add', '/add/%') as $path_end) {
        $items[$this->path . $path_end]['file'] = $this->entityInfo['admin ui']['file'];
        $items[$this->path . $path_end]['file path'] = isset($this->entityInfo['admin ui']['file path']) ? $this->entityInfo['admin ui']['file path'] : drupal_get_path('module', $this->entityInfo['module']);
      }
    }

    return $items;
  }
}
class eMappingSourceTypeUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage Source types, including adding	and removing fields and the display of fields.';
    return $items;
  }
}

class eMappingSourceUIController extends EntityDefaultUIController {
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['type'] = MENU_LOCAL_TASK;	
    // Extend the 'add' path.
    $items[$this->path . '/add'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType),
      'page callback' => 'entity_ui_bundle_add_page',
      'page arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
      'type' => MENU_LOCAL_ACTION,
    );
    $items[$this->path . '/add/%'] = array(
      'title callback' => 'entity_ui_get_action_title',
      'title arguments' => array('add', $this->entityType, $this->id_count + 1),
      'page callback' => 'entity_ui_get_bundle_add_form',
      'page arguments' => array($this->entityType, $this->id_count + 1),
      'access callback' => 'entity_access',
      'access arguments' => array('create', $this->entityType),
    );

    if (!empty($this->entityInfo['admin ui']['file'])) {
      // Add in the include file for the entity form.
      foreach (array('/add', '/add/%') as $path_end) {
        $items[$this->path . $path_end]['file'] = $this->entityInfo['admin ui']['file'];
        $items[$this->path . $path_end]['file path'] = isset($this->entityInfo['admin ui']['file path']) ? $this->entityInfo['admin ui']['file path'] : drupal_get_path('module', $this->entityInfo['module']);
      }
    }

    return $items;
  }
}

//* ***********
//* FORM STUFF
//* ***********

//********************************************
//* BEGIN - Entity Bundle Configuration Forms
//********************************************
function emapping_map_type_form($form, &$form_state, $emapping_map_type, $op = 'edit') {

  if ($op == 'clone') {
    $emapping_map_type->name .= ' (cloned)';
    $emapping_map_type->bundle = '';
  }

  $form['name'] = array(
    '#title' => t('Feature Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_map_type->name,
    '#description' => t('The human-readable name of this dH Feature type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['bundle'] = array(
    '#title' => t('Bundle Name'),
	  '#type' => 'machine_name',
    '#default_value' => isset($emapping_map_type->bundle) ? $emapping_map_type->bundle : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'emapping_layer_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this model type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#default_value' => $emapping_map_type->description,
    '#description' => t('Detailed description of this dH Feature type.'),
    '#required' => FALSE,
    '#size' => 255,
  );
    
 // $form['data']['#tree'] = TRUE;


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Layer type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_map_type_form_submit(&$form, &$form_state) {
  $emapping_map_type = entity_ui_form_submit_build_entity($form, $form_state);
  $emapping_map_type->save();
  $form_state['redirect'] = 'admin/structure/emapping_map_type';
}

function emapping_layer_type_form($form, &$form_state, $emapping_layer_type, $op = 'edit') {

  if ($op == 'clone') {
    $emapping_layer_type->name .= ' (cloned)';
    $emapping_layer_type->bundle = '';
  }

  $form['name'] = array(
    '#title' => t('Feature Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_layer_type->name,
    '#description' => t('The human-readable name of this dH Feature type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['bundle'] = array(
    '#title' => t('Bundle Name'),
	  '#type' => 'machine_name',
    '#default_value' => isset($emapping_layer_type->bundle) ? $emapping_layer_type->bundle : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'emapping_layer_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this model type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#default_value' => $emapping_layer_type->description,
    '#description' => t('Detailed description of this dH Feature type.'),
    '#required' => FALSE,
    '#size' => 255,
  );
    
 // $form['data']['#tree'] = TRUE;


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Layer type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_layer_type_form_submit(&$form, &$form_state) {
  $emapping_layer_type = entity_ui_form_submit_build_entity($form, $form_state);
  $emapping_layer_type->save();
  $form_state['redirect'] = 'admin/structure/emapping_layer_type';
}

function emapping_source_type_form($form, &$form_state, $emapping_source_type, $op = 'edit') {

  if ($op == 'clone') {
    $emapping_source_type->name .= ' (cloned)';
    $emapping_source_type->bundle = '';
  }

  $form['name'] = array(
    '#title' => t('Source Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_source_type->name,
    '#description' => t('The human-readable name of this Source type.'),
    '#required' => FALSE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['bundle'] = array(
    '#title' => t('Bundle Name'),
	  '#type' => 'machine_name',
    '#default_value' => isset($emapping_source_type->bundle) ? $emapping_source_type->bundle : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'emapping_source_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#default_value' => $emapping_source_type->description,
    '#description' => t('Detailed description of this eMapping Source type.'),
    '#required' => FALSE,
    '#size' => 255,
  );
    
 // $form['data']['#tree'] = TRUE;


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Source type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_source_type_form_submit(&$form, &$form_state) {
  $emapping_source_type = entity_ui_form_submit_build_entity($form, $form_state);
  $emapping_source_type->save();
  $form_state['redirect'] = 'admin/structure/emapping_source_type';
}

function emapping_map_form($form, &$form_state, $emapping_map, $op = 'edit') {

  if ($op == 'clone') {
    $emapping_map->name .= ' (cloned)';
    $emapping_map->bundle = '';
  }

  $form['name'] = array(
    '#title' => t('Map Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_map->name,
    '#description' => t('The human-readable name of this Map.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['title'] = array(
    '#title' => t('Map Title'),
    '#type' => 'textfield',
    '#default_value' => $emapping_map->title,
    '#description' => t('The human-readable name of this Source type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#default_value' => $emapping_map->description,
    '#description' => t('Detailed description of this eMapping Map type.'),
    '#required' => FALSE,
    '#size' => 255,
  );
  
  ctools_include('plugins');
  $plugins = ctools_get_plugins('emapping', 'Map');
  $plugopt = array();
  foreach ($plugins as $plugdef) {
    $plugopt[$plugdef['plugin_name']] = $plugdef['title'];
  }
  dpm($plugins, "info emapping map plugins");
  $form['plugin'] = array(
    '#title' => t('Plugin'),
    '#type' => 'select',
    '#options' => $plugopt,
    '#options' => $plugopt,
    '#default_value' => $emapping_map->plugin,
    '#description' => t('The rendering controller for this layer.'),
    '#required' => FALSE,
    '#multiple' => FALSE,
  );
    
 // $form['data']['#tree'] = TRUE;
  field_attach_form('emapping_map', $emapping_map, $form, $form_state);


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Map'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_map_form_submit(&$form, &$form_state) {
  $emapping_map = entity_ui_form_submit_build_entity($form, $form_state);
  $emapping_map->save();
  $form_state['redirect'] = 'admin/content/emapping_map';
}
//********************************************
//* END - Entity Bundle Configuration Forms
//********************************************

//***************************
//* BEGIN - Data Entry Forms
//***************************

/**
 * Layer Source Edit Form
 */
function emapping_source_form($form, &$form_state, $emapping_source, $op = 'edit') {

  if ($op == 'clone') {
    $emapping_source->name .= ' (cloned)';
    $emapping_source->bundle = '';
  }
  //drupal_set_message("Original object: " . print_r((array)$emapping_source,1));

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_source->name,
    '#description' => t('Name'),
    '#required' => TRUE,
    '#size' => 30,
  );  
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => $emapping_source->description,
    '#description' => t('Description'),
    '#required' => FALSE,
    '#size' => 255,
  );  
  // Machine-readable type name.
  $form['bundle'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($emapping_source->bundle) ? $emapping_source->bundle : '',
    '#maxlength' => 32,
    '#attributes' => array('disabled' => 'disabled'),
    '#machine_name' => array(
      'exists' => 'emapping_source_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this model type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['data_uri'] = array(
    '#title' => t('Data or Location of source'),
    '#type' => 'textarea',
    '#default_value' => $emapping_source->data_uri,
    '#description' => t('URI or Text data, WKT or structured such as JSON.'),
    '#required' => FALSE,
    '#size' => 5,
  );
  if (trim($emapping_source->machine_name) == '') {
    $emapping_source->machine_name = str_replace(' ', '_', strtolower($emapping_source->name ));
  }
  $form['machine_name'] = array(
    '#title' => t('Machine Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_source->machine_name,
    '#description' => t('The unique identifier of this eMapping Source type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // add configuration field
  $form['options'] = array(
    '#title' => t('Options'),
    '#type' => 'hidden',
    '#default_value' => $emapping_source->options,
    '#description' => t('Options.'),
  );
  // serializable (maybe the exportable stuff is here?)
  // use the class method to supply the form extra configs here
  field_attach_form('emapping_source', $emapping_source, $form, $form_state);
  // @todo - replace this with a ctools plugin query, with:
  //     $emapping_source->bundle - constrains overall list of options,
  //     $emapping_source->plugin - indicates the selected plugin (if bundle allows multi)
  //   When the plugin is changed, the $emapping_source->options field needs to be re-rendered
  $config = emapping_get_source_configs($emapping_source->bundle);
  //drupal_set_message("Using class: " . $config['class']);
  if (class_exists($config['class'])) {
    $src = new $config['class'];
    //drupal_set_message("Options: " . print_r($emapping_source->options,1));
    $src->init($emapping_source->options);
    //$src->init(unserialize($emapping_source->options));
    $src->buildOptionsForm($form, $form_state);
  }
  
  $form['data']['#tree'] = TRUE;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save eMapping Source type'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete type'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('emapping_source_form_submit_delete')
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_source_form_submit(&$form, &$form_state) {
  $config = emapping_get_source_configs($form_state['values']['bundle']);
  //drupal_set_message("Using class: " . $config['class']);
  if (class_exists($config['class'])) {
    $src = new $config['class'];
    $o = $src->option_definition();
    $options = array();
    //drupal_set_message("Form state Values: <pre>" . print_r($form_state['values'],1) . "</pre>");
    foreach ($o as $optname => $attributes) {
      //drupal_set_message("Looking for option $optname ");
      $options[$optname] = isset($form_state['values'][$optname]) ? $form_state['values'][$optname] : NULL;
      
    }
  }
  $form_state['values']['options'] = $options;
  $emapping_source = entity_ui_form_submit_build_entity($form, $form_state);
  //drupal_set_message("Final entity to save: <pre>" . print_r((array)$emapping_source,1) . "</pre>");
  $emapping_source->save();
  $form_state['redirect'] = 'admin/content/emapping_source';
}

/**
 * Form API submit callback for the delete button.
 */
function emapping_source_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/emapping_source/manage/' . $form_state['emapping_source']->sid . '/delete';
}


/**
 * Layer Source Edit Form
 */
function emapping_layer_form($form, &$form_state, $emapping_layer, $op = 'edit') {
  // check to see if this is to be attached to a map on save
  $p = drupal_get_query_parameters() ;
  if (isset($p['mapid'])) {
    if (intval($p['mapid']) > 0) {
      $emapping_layer->mapid = $p['mapid'];
    }
  }
  if ($op == 'clone') {
    $emapping_layer->name .= ' (cloned)';
    $emapping_layer->bundle = '';
  }

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_layer->name,
    '#description' => t('Name'),
    '#required' => TRUE,
    '#size' => 30,
  );  
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => $emapping_layer->title,
    '#description' => t('Title'),
    '#required' => TRUE,
    '#size' => 30,
  );  
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => $emapping_layer->description,
    '#description' => t('Description'),
    '#required' => FALSE,
    '#size' => 255,
  );  
  // Machine-readable type name.
  $form['bundle'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($emapping_layer->bundle) ? $emapping_layer->bundle : '',
    '#maxlength' => 32,
    '#attributes' => array('disabled' => 'disabled'),
    '#machine_name' => array(
      'exists' => 'emapping_layer_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this model type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  if (trim($emapping_layer->machine_name) == '') {
    $emapping_layer->machine_name = str_replace(' ', '_', strtolower($emapping_layer->name ));
  }
  
  $plugins = ctools_get_plugins('emapping', 'Component');
  $plugopt = array();
  foreach ($plugins as $plugdef) {
    $plugopt[$plugdef['plugin_name']] = $plugdef['title'];
  }
  dpm($plugins, "info emapping component plugins");
  $form['plugin'] = array(
    '#title' => t('Plugin'),
    '#type' => 'select',
    '#options' => $plugopt,
    '#default_value' => $emapping_map->plugin,
    '#description' => t('The rendering controller for this layer.'),
    '#required' => FALSE,
    '#multiple' => FALSE,
  );
  
  // @todo use the class method to supply the form extra configs here
  field_attach_form('emapping_layer', $emapping_layer, $form, $form_state);
  // @todo - over-ride config form with a ctools plugin query,
  // if layer source changes, of if layer plugin is changed, the $emapping_layer->options field needs to be re-rendered
  
  // **************************** 
  // BEGIN emapping_layer plugin 
  // **************************** 
  //     $emapping_map->bundle - constrains overall list of options,
  //     $emapping_layer->bundle - secondary constraint of plugins,
  //     $emapping_layer->plugin - indicates the selected plugin (if bundle allows multi)
  /*
  $config = emapping_get_layer_configs($emapping_layer->bundle);
  //drupal_set_message("Using class: " . $config['class']);
  if (class_exists($config['class'])) {
    $src = new $config['class'];
    //drupal_set_message("Options: " . print_r($emapping_layer->options,1));
    $src->init($emapping_layer->options);
    //$src->init(unserialize($emapping_layer->options));
    $src->buildOptionsForm($form, $form_state);
  }
  // **************************** 
  // END emapping_layer plugin 
  // **************************** 
  */
  
  $form['machine_name'] = array(
    '#title' => t('Machine Name'),
    '#type' => 'textfield',
    '#default_value' => $emapping_layer->machine_name,
    '#description' => t('The unique identifier of this eMapping Source type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // this will attach such things as the entityreference to the source object
  // TODO: apply a filter based on the Layer Objects read_formats array, and the Sources getFormats() output
    // a given layer should only show source options where Layer::read_formats intersects source getFormats()
  field_attach_form('emapping_layer', $emapping_layer, $form, $form_state);
  
  $form['data']['#tree'] = TRUE;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save eMapping Source type'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete type'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('emapping_layer_form_submit_delete')
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function emapping_layer_form_submit(&$form, &$form_state) {
  $emapping_layer = entity_ui_form_submit_build_entity($form, $form_state);
  $emapping_layer->save();
  if (property_exists($emapping_layer,'mapid')) {
    // add layer reverse reference to the indicated map
    $map = entity_load_single('emapping_map', $emapping_layer->mapid);
    if ($map) {
      $map->maplayer['und'][] = array(
        'target_id' => $emapping_layer->lid, 
      );
      if ($emapping_layer->lid > 0) {
        $map->save();
      } else {
        drupal_set_message('Invalid lid, will not save layer');
      }
    } else {
      drupal_set_message("Could not locate map with id " . $emapping_layer->mapid);
    }
  }
  $form_state['redirect'] = 'admin/content/emapping_layer';
}

/**
 * Implements hook_entity_delete for special handling if necessary.
 */
function emapping_entity_delete($entity, $type) {
  switch ($type) {
    case 'emapping_layer':
      emapping_layer_delete($entity);
    break;
  }
}

/**
 * Find all entity references to this layer and delete them.
 */
function emapping_layer_delete($layer) {
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'emapping_map');
  $efq->fieldCondition('maplayer', 'target_id', $layer->lid, '=');
  $results = $efq->execute();
  if (isset($results['emapping_map'])) {
    foreach ($results['emapping_map'] as $map) {
      if (property_exists($map, 'mid')) {
        $map = entity_load_single('emapping_map', $map->mid);
        if ($map) {
          $save = FALSE;
          foreach ($map->maplayer['und'] as $key => $l) {
            if ($l['target_id'] == $layer->lid) {
              unset($map->maplayer['und'][$key]);
              $save = TRUE;
            }
          }
          if ($save) {
            $map->save();
          }
        }
      } 
    }
  }
}

function emapping_layer_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/emapping_layer/manage/' . $form_state['emapping_layer']->lid . '/delete';
}

